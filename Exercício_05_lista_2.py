potencia = int(input('Qual a potência da lâmpada em watts? '))
largura = int(input('Qual a largura do cômodo em metros?' ))
comprimento = int(input('Qual o comprimento do cômodo em metros?' ))

area = largura * comprimento
p_total = area * 18
n_lamp = p_total/potencia

print(f'O número de lâmpadas para iluminar o cômodo é: {round(n_lamp)}')
