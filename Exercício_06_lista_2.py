largura = int(input('Qual a largura do cômodo em metros? ' ))
comprimento = int(input('Qual o comprimento do cômodo em metros? ' ))
altura = int(input('Qual a altura do cômodo? '))

perimetro = ((largura*2) + (comprimento*2))
area = (perimetro * altura)
n_caixas = (area / 1.5)

print(f'O número de caixas de azulejo para revistir o cômodo é:',(n_caixas))
